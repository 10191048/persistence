package com.arifin_10191048.persistence;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Context;
import android.content.SharedPreferences;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.prefs.Preferences;

public class MainActivity extends AppCompatActivity {

//    Deklarasi variabel pendukung
    private TextView Hasil;
    private EditText Masukan;
    private Button Eksekusi;

//    Deklarasi dan Inisiasi SharedPreferences
    private SharedPreferences preferences;

//    Digunakan untuk konfigurasi SharedPreferences
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Masukan = findViewById(R.id.input);
        Hasil = findViewById(R.id.output);
        Eksekusi = findViewById(R.id.save);

//        Membuat file baru beserta modifirnya
        preferences = getSharedPreferences("Belajar_SharedPreferences", Context.MODE_PRIVATE);

        Eksekusi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getData();
                Toast.makeText(getApplicationContext(), "Data Tersimpan", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getData() {
        String getKonten = Masukan.getText().toString();
        editor = preferences.edit();
        editor.putString("data_saya",getKonten);
        editor.apply();
        Hasil.setText("Output Data: "+preferences.getString("data_saya",null));
    }
}